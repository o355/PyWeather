# PyWeather Config Setup - Version 1.0.2
# (c) 2017-2018, o355

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import configparser
import traceback
import time

config = configparser.ConfigParser()
config.read("storage//config.ini")

# Verbosity and all that fun stuff isn't available here.
# If the config isn't s   et up, and by default, verbosity is off
# why should I code it in?

print("Would you like to set up (or reset) PyWeather's config?",
      "Yes or No.", sep="\n")
cd_confirmation = input("Input here: ").lower()
if cd_confirmation == "yes":
    try:
        provisioned = config['USER']['configprovisioned']
        print("Your config file is already provisioned! Would you still",
              "like to have your config reprovisioned?",
              "Yes or No.", sep="\n")
        keepprovisioning = input("Input here: ").lower()
        if keepprovisioning == "yes":
            print("Re-provisioning your config...")
        elif keepprovisioning == "no":
            print("Not re-provisioning your config.",
                  "Press enter to exit.")
            input()
            sys.exit()
    except:
        print("Setting up your config...")

    try:
        config.add_section("GEOCODER API")
    except configparser.DuplicateSectionError:
        print("Failed to add the Geocoder API section.")

    try:
        config.add_section("FAVORITE LOCATIONS")
    except configparser.DuplicateSectionError:
        print("Failed to add the favorite locations section.")
    try:
        config.add_section("HURRICANE")
    except configparser.DuplicateSectionError:
        print("Failed to add the hurricane section.")

    try:
        config.add_section("FIRSTINPUT")
    except configparser.DuplicateSectionError:
        print("Failed to add the firstinput section.")
    try:
        config.add_section('SUMMARY')
    except configparser.DuplicateSectionError:
        print("Failed to add the summary section.")

    try:
        config.add_section('VERBOSITY')
    except configparser.DuplicateSectionError:
        print("Failed to add the verbosity section.")

    try:
        config.add_section('TRACEBACK')
    except configparser.DuplicateSectionError:
        print("Failed to add the traceback section.")

    try:
        config.add_section('UI')
    except configparser.DuplicateSectionError:
        print("Failed to add the UI section.")

    try:
        config.add_section('PREFETCH')
    except configparser.DuplicateSectionError:
        print("Failed to add the prefetch section.")

    try:
        config.add_section('UPDATER')
    except configparser.DuplicateSectionError:
        print("Failed to add the updater section.")

    try:
        config.add_section('KEYBACKUP')
    except configparser.DuplicateSectionError:
        print("Failed to add the keybackup section.")

    try:
        config.add_section('PYWEATHER BOOT')
    except configparser.DuplicateSectionError:
        print("Failed to add the PyWeather Boot section.")

    try:
        config.add_section('USER')
    except configparser.DuplicateSectionError:
        print("Failed to add the user section.")

    try:
        config.add_section('CACHE')
    except configparser.DuplicateSectionError:
        print("Failed to add the cache section.")

    try:
        config.add_section('RADAR GUI')
    except configparser.DuplicateSectionError:
        print("Failed to add the Radar GUI section.")

    config['SUMMARY']['sundata_summary'] = 'False'
    config['SUMMARY']['almanac_summary'] = 'False'
    config['SUMMARY']['showalertsonsummary'] = 'True'
    config['SUMMARY']['showtideonsummary'] = 'False'
    config['SUMMARY']['showyesterdayonsummary'] = 'False'
    config['VERBOSITY']['verbosity'] = 'False'
    config['VERBOSITY']['json_verbosity'] = 'False'
    config['VERBOSITY']['setup_verbosity'] = 'False'
    config['VERBOSITY']['setup_jsonverbosity'] = 'False'
    config['VERBOSITY']['updater_verbosity'] = 'False'
    config['VERBOSITY']['updater_jsonverbosity'] = 'False'
    config['TRACEBACK']['tracebacks'] = 'False'
    config['TRACEBACK']['setup_tracebacks'] = 'False'
    config['TRACEBACK']['updater_tracebacks'] = 'False'
    config['UI']['show_entertocontinue'] = 'True'
    config['UI']['detailedinfoloops'] = '6'
    config['UI']['forecast_detailedinfoloops'] = '5'
    config['UI']['show_completediterations'] = 'False'
    config['UI']['extratools_enabled'] = 'False'
    config['UI']['hourlyinfo_iterations'] = '2'
    config['UI']['forecastinfo_iterations'] = '3'
    config['UI']['hurricaneinfo_iterations'] = '2'
    config['UI']['tideinfo_iterations'] = '6'
    config['UI']['historicalhourly_iterations'] = '2'
    config['UI']['yesterdayhourly_iterations'] = '2'
    config['UI']['alertsus_iterations'] = '1'
    config['UI']['alertseu_iterations'] = '2'
    config['PREFETCH']['10dayfetch_atboot'] = 'False'
    config['PREFETCH']['yesterdaydata_atboot'] = 'False'
    config['UPDATER']['autocheckforupdates'] = 'False'
    config['UPDATER']['show_updaterreleasetag'] = 'False'
    config['KEYBACKUP']['savedirectory'] = 'backup//'
    config['PYWEATHER BOOT']['validateapikey'] = 'True'
    config['UPDATER']['showReleaseNotes'] = 'True'
    config['UPDATER']['showReleaseNotes_uptodate'] = 'False'
    config['UPDATER']['showNewVersionReleaseDate'] = 'True'
    config['USER']['configprovisioned'] = 'True'
    config['CACHE']['enabled'] = 'True'
    config['CACHE']['alerts_cachedtime'] = '5'
    config['CACHE']['current_cachedtime'] = '10'
    config['CACHE']['36hourly_cachedtime'] = '60'
    config['CACHE']['tendayhourly_cachedtime'] = '60'
    config['CACHE']['forecast_cachedtime'] = '60'
    config['CACHE']['almanac_cachedtime'] = '240'
    config['CACHE']['sundata_cachedtime'] = '480'
    config['CACHE']['tide_cachedtime'] = '480'
    config['CACHE']['hurricane_cachedtime'] = '180'
    config['CACHE']['yesterday_cachedtime'] = '720'
    config['RADAR GUI']['radar_imagesize'] = 'normal'
    config['RADAR GUI']['bypassconfirmation'] = 'False'
    config['GEOCODER API']['customkey_enabled'] = 'False'
    config['GEOCODER API']['customkey'] = 'None'
    config['GEOCODER API']['customkey_w3w'] = 'None'
    config['GEOCODER API']['skipverification'] = 'False'
    config['PREFETCH']['hurricanedata_atboot'] = 'False'
    config['FIRSTINPUT']['geoipservice_enabled'] = 'False'
    config['FIRSTINPUT']['allow_pwsqueries'] = 'True'
    config['FIRSTINPUT']['allow_airportqueries'] = 'True'
    config['FIRSTINPUT']['allow_latlonqueries'] = 'True'
    config['FIRSTINPUT']['allow_w3wqueries'] = 'False'
    config['HURRICANE']['enablenearestcity'] = 'False'
    config['HURRICANE']['enablenearestcity_forecast'] = 'False'
    config['HURRICANE']['api_username'] = 'pyweather_proj'
    config['HURRICANE']['nearestcitysize'] = 'medium'
    config['FAVORITE LOCATIONS']['enabled'] = 'True'
    config['FAVORITE LOCATIONS']['favloc1'] = 'None'
    config['FAVORITE LOCATIONS']['favloc2'] = 'None'
    config['FAVORITE LOCATIONS']['favloc3'] = 'None'
    config['FAVORITE LOCATIONS']['favloc4'] = 'None'
    config['FAVORITE LOCATIONS']['favloc5'] = 'None'
    config['FAVORITE LOCATIONS']['favloc1_data'] = 'None'
    config['FAVORITE LOCATIONS']['favloc2_data'] = 'None'
    config['FAVORITE LOCATIONS']['favloc3_data'] = 'None'
    config['FAVORITE LOCATIONS']['favloc4_data'] = 'None'
    config['FAVORITE LOCATIONS']['favloc5_data'] = 'None'

    try:
        with open('storage//config.ini', 'w') as configfile:
            config.write(configfile)
    except:
        print("An error occurred when attempting to write to the config file.",
              "Make sure that you have proper permissions on the config file.",
              "Otherwise, consider creating a bug report at gitlab.com/o355.", sep="\n")
        traceback.print_exc()
        print("Press enter to exit.")
        input()
        sys.exit()
    try:
        open('updater//versioninfo.txt', 'w').close()
        with open("updater//versioninfo.txt", 'a') as out:
            out.write("v1.0.2")
            out.close()
    except:
        print("Couldn't write the versioninfo file. This may cause issues with PyWeather down the road.")

    print("All done! Press enter to exit.")
    input()
    sys.exit()
elif cd_confirmation == "no":
    print("Not provisioning your config file.",
          "Press enter to exit.", sep="\n")
    input()
    sys.exit()
else:
    print("Couldn't understand input. Not provisioning your config file.",
          "You can always relaunch this script if you'd like to provison your config file.",
          "Press enter to exit.", sep="\n")
    input()
    sys.exit()
