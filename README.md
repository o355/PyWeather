## Welcome to PyWeather (1.0.3)!

Welcome to PyWeather, the fun way to check the weather in a terminal. Thanks for being here!

PyWeather is the culmination of thousands of hours of work poured into a silly little project that got more advanced over time.

I hope that you can enjoy PyWeather as much as I enjoy making PyWeather, so, let's get started!

## Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

PyWeather Spaghet-o-meter: **1000/10**

PyWeather is the most aggregious crime of spaghetti code I've committed. 11,000 lines rolled into one huge file.

It's 100% spaghetti. It's awful spaghetti. Yes, there's a config file. That's it though. Hundreds of lines taken up by config stuff, thousands for weather parsing, loads of copypasta code all over the place. All of the functions should have been handled by separate files. Weather parsing by an object-oriented library. Config parsing by literally anything but hundreds of config variables with try/except blocks and copypasta code everywhere.

No organization, no reusability, absolutely nothing. It's the worst code I've ever written - by far.

## Notice - PyWeather has stopped working, and will no longer be developed.
Over the past two years I've had an absolute blast developing and working on PyWeather. However, the API that PyWeather relies on is no longer functional.

I've moved on to bigger and better projects since the start of 2019, and as a result, won't be working on PyWeather anymore. v2.0.0 is cancelled.

Thank you for your interest in PyWeather. The readme instructions are moot, as of course, PyWeather won't work.

The PyWeather Archives will be worked on soon, so please do look forward to that!

## Requirements
To run PyWeather, you'll need:
* A computer (Windows, OS X, and most Linux distros)
* An internet connection
* Python 3 & PIP for Python 3 (download at https://python.org/downloads).
* ~5-10 MB of disk space

## Download/Setup
To download PyWeather, click the "Tags" button at the top of the page. You should see the latest version of PyWeather at the top, and a Download link just below. Click the first download link.

By default, the .zip file has PyWeather contained in a folder (pyweather-v1.0.2 for instance). If you would rather not have all the files be contained in such a folder, click the second download link for the "Dirless Edition".

### Git Setup
If you'd rather, you can also download PyWeather with Git, using the following commands below.

```
git clone https://gitlab.com/o355/pyweather.git
cd pyweather
git checkout v1.0.3
```


Once you've downloaded PyWeather, you can run setup.py, either by double-clicking it, or by running `python3 setup.py` in a terminal, depending on your OS. The setup file will guide you through getting the right libraries, API keys, and configuring PyWeather to your liking.

Once setup is complete, you can run pyweather.py, either by double-clicking it, or by running `python3 pyweather.py` in a terminal, depending on your OS. That's all there is to it.

### Indev code
If you don't care about stability, don't mind PyWeather breaking from time-to-time, but really want the latest features, you can run indev code.

Running indev code is seriously not recommended. You'll probably have to fix your config file manually over and over again, deal with instability, and the things usually associated with indev code.

To download the latest indev code, click the icon with a cloud and a downwards facing arrow. Choose the format you'd like to download the indev code with.

### Indev code - Git Setup
If you're going to run indev code, it's highly recommended to use Git, as it's easier to fetch the latest code. Run these commands to get things set up:

```
git clone https://gitlab.com/o355/pyweather.git
cd pyweather
```

Run the setup file as described above, and then run PyWeather. Just make sure you run `git pull` on a nightly basis :)

## Disclaimer
PyWeather **should not** be used during severe weather conditions, and should not be used to tell the public about weather conditions during a severe weather event.

Please listen to your local authorities and weather services during a severe event.

## What's PyWeather?
PyWeather is a Python script that fetches the weather using Wunderground's API. I made PyWeather as a solution to a few things:
* The lack of script-based weather software that was coded in Python, and that relied on a decent enough API
* The long loading times (and loads of tracking/adverts) when visiting normal weather websites
* Having the ability to check my weather in a terminal.

As such, PyWeather was born. After more than 77,000 lines of code getting added & removed by 7 contributers, this is what happened.

## What features does PyWeather have?
PyWeather has lots of them, and the list is ever expanding. Here's the present feature list:
* Easy setups. The one-run setup script will install necessary libraries, guide you through API key setup, and let you configure PyWeather to your liking.
* PyWeather uses Wunderground's API, meaning you get accurate, hyper-local weather across the world.
* PyWeather has a lot of configuration options - and I mean a lot. You can configure over 50 settings to fine-tune how PyWeather works for you.
* PyWeather is simple to use. I've tried to make PyWeather appeal to both people who don't know what Python is, to people who know how to use software. Either way, you can use PyWeather easily.
* Lots of data types. All these data types as a matter of a fact!
	* Current information
	* Alerts information (US/EU only due to Wunderground's API limits)
	* 36-hour and 10-day hourly forecasts
	* 10-day forecasts
	* Almanac information
	* Astronomy (sunrise/sunset/moonrise/moonset + moon) information
	* Historical weather information, including hourly weather for a historical date.
	* Radar information
	* Yesterday's weather
	* Tide data
	* Hurricane data
	
* PyWeather is modular. You can easily turn on or turn off features to your own liking.
* PyWeather has a fully-featured updater built-in. PyWeather updates are just as easy as updating your computer or phone.
* PyWeather has a built-in geoIP service, which can detect your approximate location for even faster weather access. However, it's not 100% accurate on mobile connections.
* PyWeather supports the viewing of weather data for individual Wunderground Personal Weather Stations, and any airport weather station on Earth.
* PyWeather can store 5 favorite locations of yours, and can easily call up those favorite locations when you boot up PyWeather.
* PyWeather supports using what3words & direct lat/lon queries to get weather for.
* PyWeather can tell you which city a tropical storm is closest to, at a range of up to 300km.
* PyWeather has a built-in caching system. If you leave PyWeather open for some time, and come back to check a weather data type, PyWeather will fetch the latest weather data. You can fine-tine how long each data type is cached, along with enabling/disabling the cache.
* PyWeather is optimized for a terminal UI. Enter to continue prompts are used throughout PyWeather, and the amount of data shown is optimized for all terminal types. You can also fine-tune how the UI works to your liking.

## Why actually use PyWeather over Wunderground, or other apps?
PyWeather has lots of advantages over using websites on your desktop, or even weather apps on mobile (PyWeather is compatible with Termux for Android, and other SSH apps of course)

**On a desktop:**
* PyWeather is faster than visiting weather websites, and sometimes even faster than Googling the weather for a location!
* PyWeather has no ads (or annoying articles or autoplaying videos), so you get distraction-free weather.
* PyWeather has a ton of weather data to display - and it's much detailed and easier to access than on weather websites.

**On a phone:**
* PyWeather uses much, much less power than other weather apps - especially apps like The Weather Channel, AccuWeather, and even Wunderground's app.
* PyWeather can easily save mobile data with default settings versus other weather apps, since ads aren't being loaded. If you more carefully tune PyWeather's settings, you can save even more data.
* PyWeather is usually faster than native weather apps - Especially when using favorite locations and the `fl:x` shortcut.
* On the subject of speed, PyWeather is a lot faster on a slow mobile connection. It managed to load (albeit in about 30 seconds) on a fringe 1X connection. Weather apps usually time out or take ages to load under these conditions.

**Other advantages:**
* PyWeather is free open source software, uses completely open-source libraries, and PyWeather is licensed under GNU's GPL v3 license! Richard Stallman would approve.
* PyWeather can handly beat using websites and other services when on a slow internet connection. In PyWeather's default configuration, it only took 6 seconds for PyWeather to boot up on a simulated 28 kbps internet connection, and 20-30 seconds on a real world, fringe 1X connection (Verizon). Tl;dr: You can use PyWeather on dial-up.
* You'll look cool being that one person using a terminal to check your weather like it's the 80s.
* PyWeather lets you easily access many data types in one place - where as on other weather sites, this data may not be easy to access, or nonexistant at all.
* PyWeather **doesn't track you!** I don't even know how many people are using PyWeather on a daily basis. I don't have code that "phones home", and I don't have code that relays back **any usage data**. However, do note that Wunderground does likely know your IP and basic system information about you from requesting their API, and the same goes for Google's geocoder that PyWeather uses. In the end, the amount of data you're giving to Wunderground and Google's geocoder by using PyWeather is much less than what you give to advertisers and trackers when visiting other weather sites.

## Contributors
Thanks to these awesome people, they've helped out PyWeather by coding in bug fixes, new features, reporting bugs, and suggesting new stuff.

* ModoUnreal (BadassBanana) - Awesome contributor - Has made 10 pull requests, 5 of which were merged, and coded in 2 new features/1 data type
* creepersbane (Benjamin Urquhart) - Awesome contributor -  Has made 6 reports (3 of which required code changes), and 1 pull request.
* theletterandrew (Andrew Philips) - Awesome contributor - Has made 2 reports, which required code changes, has made 1 pull requested which was merged, and coded in 1 new feature
* gsilvapt - Contributor - Has made 1 report, which required code changes
* DiSiqueira - Contributor - Has made 1 report.
* MaT1g3R - Contributor - Has made 1 pull request.

## Sharing
I've, among others have poured in hundreds of hours into making PyWeather what it is, and I plan to spend hundreds of more hours refining PyWeather.

If you'd like, please consider telling your friends, social media followers, whomever about PyWeather. Every star, fork, and user keeps me motivated to keep working on PyWeather.
