# PyWeather Configuration Updater - version 1.0.3
# (c) 2017-2018, o355

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import configparser
import traceback
import subprocess

try:
    versioninfo = open("updater//versioninfo.txt")
    versioninfo2 = versioninfo.read()
    versioninfo.close()
    manualversioncheck = False
except:
    manualversioncheck = True
    # Manual versioninfo check if the file couldn't be found

# New function to install a library
def newlibinstaller(library):
    # Check for the library we're installing
    if library == "halo":
        try:
            import halo
            haloInstalled = True
        except ImportError:
            haloInstalled = False

        if haloInstalled is True:
            print(
                "For PyWeather 0.6.3 beta and above, a new library called 'halo' is required for PyWeather to operate.",
                "This library controls the new loaders that are present in PyWeather. The good news is that you already have",
                "halo installed. Skipping automatic install of halo.", sep="\n")
            print("")
            return
        elif haloInstalled is False:
            print(
                "For PyWeather 0.6.3 beta and above, a new library called 'halo' is required for PyWeather to operate.",
                "This library controls the new loaders that are present in PyWeather. Unfortunately, halo is not installed.",
                "Would you like to perform an automatic install of halo? Yes or No.", sep='\n')
            installhalo_input = input("Input here: ").lower()
            if installhalo_input == "yes":
                print("Now installing halo using pip's built-in installer.")
                subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'halo'])
                print("Now checking if Halo has been installed...")
                try:
                    import halo
                    print("")
                    print("Halo has been successfully installed!")
                    return
                except ImportError:
                    print("")
                    print("When attempting an automatic install, Halo failed to install properly.",
                          "To use the latest version of PyWeather, you'll need to manually use a command line",
                          "and manually install halo. The command to do this is `pip3 install halo` on most",
                          "platforms. If an error occurred use a search engine to try and fix the error.", sep="\n")
                    return

            elif installhalo_input == "no":
                print("To use the latest version of PyWeather, you'll need to install halo in a command line.",
                      "The command to do this is `pip3 install halo`, and it should work on most platforms.", "",
                      sep="\n")
                print("")
                return
            else:
                print(
                    "Your input could not be understood. As a precaution the automatic install will not be carried out.",
                    "To use the latest version of PyWeather, you'll need to install halo in a command line.",
                    "The command to do this is `pip3 install halo`, and it should work on most platforms.", "",
                    sep="\n")
                print("")
                return

    elif library == "click":
        try:
            import click
            clickInstalled = True
        except:
            clickInstalled = False

        if clickInstalled is True:
            print("For PyWeather 1.0.0 and above, a new library called 'click' is required for PyWeather to operate.",
                  "This library presently controls the progress bar for the new updater. The good news is that you already have",
                  "click installed. Skipping automatic install of click.", sep="\n")
            print("")
            return
        elif clickInstalled is False:
            print(
                "For PyWeather 1.0.0 and above, a new library called 'click' is required for PyWeather to operate.",
                "This library presently controls the progress bar for the new updater. Unfortunately, click is not installed.",
                "Would you like to perform an automatic install of click? Yes or No.", sep="\n")
            installclick_input = input("Input here: ").lower()
            if installclick_input == "yes":
                print("Now installing click using pip's built-in installer.")
                subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'click'])
                print("Now checking if Click has been installed...")
                try:
                    import click
                    print("")
                    print("Click has been successfully installed!")
                    print("")
                    return
                except ImportError:
                    print("")
                    print("When attempting an automatic install, Click failed to install properly.",
                          "To use the latest version of PyWeather, you'll need to manually use a command line",
                          "and manually install click. The command to do this is `pip3 install click` on most",
                          "platforms. If an error occurred use a search engine to try and fix the error.", sep="\n")
                    print("")
            elif installclick_input == "no":
                print("To use the latest version of PyWeather, you'll need to install click in a command line.",
                      "The command to do this is `pip3 install click`, and it should work on most platforms.", "",
                      sep="\n")
                print("")
                return
            else:
                print(
                    "Your input could not be understood. As a precaution the automatic install will not be carried out.",
                    "To use the latest version of PyWeather, you'll need to install click in a command line.",
                    "The command to do this is `pip3 install click`, and it should work on most platforms.", "",
                    sep="\n")
                print("")
                return

    elif library == "certifi":
        try:
            import certifi
            certifiInstalled = True
        except:
            certifiInstalled = False

        if certifiInstalled is True:
            print("For PyWeather 1.0.0 and above, a new library called 'certifi' is required for PyWeather to operate.",
                  "This library prevents issues with HTTPS and geocoders. The good news is that you already have",
                  "certifi installed. Skipping automatic install of certifi.", sep="\n")
            print("")
            return
        elif certifiInstalled is False:
            print(
                "For PyWeather 1.0.0 and above, a new library called 'certifi' is required for PyWeather to operate.",
                "This library prevents issues with HTTPS and geocoders. Unfortunately, certifi is not installed.",
                "Would you like to perform an automatic install of certifi? Yes or No.", sep="\n")
            installcertifi_input = input("Input here: ").lower()
            if installcertifi_input == "yes":
                print("Now installing certifi using pip's built-in installer.")
                subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'certifi'])
                print("Now checking if Certifi has been installed...")
                try:
                    import certifi
                    print("")
                    print("Certifi has been successfully installed!")
                    print("")
                    return
                except ImportError:
                    print("")
                    print("When attempting an automatic install, Certifi failed to install properly.",
                          "To use the latest version of PyWeather, you'll need to manually use a command line",
                          "and manually install certifi. The command to do this is `pip3 install certifi` on most",
                          "platforms. If an error occurred use a search engine to try and fix the error.", sep="\n")
                    print("")
            elif installcertifi_input == "no":
                print("To use the latest version of PyWeather, you'll need to install certifi in a command line.",
                      "The command to do this is `pip3 install certifi`, and it should work on most platforms.", "",
                      sep="\n")
                print("")
                return
            else:
                print(
                    "Your input could not be understood. As a precaution the automatic install will not be carried out.",
                    "To use the latest version of PyWeather, you'll need to install certifi in a command line.",
                    "The command to do this is `pip3 install certifi`, and it should work on most platforms.", "",
                    sep="\n")
                print("")
                return
    else:
        return

def cachetimefix():
    global config
    print("We noticed that your options for 1.5 day hourly and almanac cache times are quite high. We presume",
          "this is because of the critical cache bug in PyWeather 0.6.3 beta.",
          "Would you like us to:",
          "[1] Divide your cache options by 60 for 1.5 day hourly and almanac for proper functioning",
          "[2] Set the cache options for 1.5 day hourly and almanac to their default",
          "[3] Do nothing", sep="\n")
    cachetimefix_input = input("Input here: ")
    if cachetimefix_input == "1":
        print("Fixing cache variables.")
        old_36hrcachetime = int(config['CACHE']['threedayhourly_cachedtime'])
        old_36hrcachetime = old_36hrcachetime / 60
        config['CACHE']['threedayhourly_cachedtime'] = str(old_36hrcachetime).replace('.0', "")

        old_almanaccachetime = int(config['CACHE']['almanac_cachedtime'])
        old_almanaccachetime = old_almanaccachetime / 60
        config['CACHE']['almanac_cachedtime'] = str(old_almanaccachetime).replace('.0', "")
        print("All done!")
        print("")
    elif cachetimefix_input == "2":
        print("Resetting cache options for 1.5 day hourly/almanac to their defaults.")
        config['CACHE']['threedayhourly_cachedtime'] = '60'
        config['CACHE']['almanac_cachedtime'] = '240'
        print("All done!")
        print("")
    elif cachetimefix_input == "3":
        print("Not doing anything. This may result in erratic behavior with the cache for",
              "1.5 day and almanac data. You can always modify your config file in case",
              "something unexpected occurs.", sep="\n")
        print("")
    else:
        print("Could not understand your input. Defaulting to not doing anything.",
              "This may result in erratic behavior with the cache for 1.5 day and",
              "almanac data. You can always modify your config file in case something",
              "unexpected occurs.", sep="\n")
        print("")


# Define build numbers for different versions of PyWeather. Later in time I'll switch to a dictionary and for loop
# to cover this part of the code

# Remove newlines & tabs from versioninfo2

if manualversioncheck is False:
    versioninfo2 = versioninfo2.replace("\n", "").replace("\t", "")

    if versioninfo2 == "0.6 beta":
        buildnumber = 60
    elif versioninfo2 == "0.6.0.1 beta":
        buildnumber = 60
    elif versioninfo2 == "0.6.1 beta":
        buildnumber = 61
    elif versioninfo2 == "0.6.2 beta":
        buildnumber = 62
    elif versioninfo2 == "0.6.3 beta":
        buildnumber = 63
    elif versioninfo2 == "v1.0.0":
        buildnumber = 100
    elif versioninfo2 == "v1.0.1":
        buildnumber = 101
    elif versioninfo2 == "v1.0.2":
        buildnumber = 102
    elif versioninfo2 == "v1.0.3":
        buildnumber = 103
    else:
        # If we have the versioninfo file but no matching data, launch the manual version check.
        manualversioncheck = True


if manualversioncheck is True:
    print("Your versioncheck file couldn't be found. Below, please enter a number",
          "which corresponds to the version of PyWeather you're updating from.",
          "[0] v0.5.2.1 beta and earlier",
          "[1] v0.6 beta or v0.6.0.1 beta",
          "[2] v0.6.1 beta or v0.6.1 beta-https",
          "[3] v0.6.2 beta",
          "[4] v0.6.3 beta",
          "[5] v1.0.0",
          "[6] v1.0.1",
          "[7] v1.0.2",
          "[8] v1.0.3", sep="\n")
    versionselect = input("Input here: ").lower()
    if versionselect == "0":
        print("You'll need to completely reinstall PyWeather due to the way the new config system works.",
              "Instructions are available on PyWeather's GitLab wiki.", sep="\n")
        input()
        sys.exit()
    elif versionselect == "1":
        print("Updating PyWeather using version identifier: 0.6 beta")
        versioninfo2 = "0.6 beta"
        buildnumber = 60
    elif versionselect == "2":
        print("Updating PyWeather using version identifier: 0.6.1 beta")
        versioninfo2 = "0.6.1 beta"
        buildnumber = 61
    elif versionselect == "3":
        print("Updating PyWeather using version identifier: 0.6.2 beta")
        versioninfo2 = "0.6.2 beta"
        buildnumber = 62
    elif versionselect == "4":
        print("Updating PyWeather using version identifier: 0.6.3 beta")
        versioninfo2 = "0.6.3 beta"
        buildnumber = 63
    elif versionselect == "5":
        print("Updating PyWeather using version identifier: v1.0.0")
        versioninfo2 = "v1.0.0"
        buildnumber = 100
    elif versionselect == "6":
        print("Updating PyWeather using version identifier: v1.0.1")
        versioninfo2 = "v1.0.1"
        buildnumber = 101
    elif versionselect == "7":
        print("Updating PyWeather using version identifier: v1.0.2")
        versioninfo2 = "v1.0.2"
        buildnumber = 102
    elif versionselect == "8":
        print("Updating PyWeather using version identifier: v1.0.3")
    else:
        print("Could not understand your input. As a result, this script is now quitting.",
              "No config changes have been made. Please rerun this script and properly enter your",
              "version information for a configuration update. Press enter to exit.", sep="\n")
        input()
        sys.exit()

# Define where to look for the config file. In very early versions of PyWeather the config file was in the main directory.
config = configparser.ConfigParser()
if buildnumber == 60:
    config.read("config.ini")
else:
    config.read("storage//config.ini")

# If we're up-to-date, stop the script
if buildnumber == 103:
    print("Your copy of PyWeather is up-to-date. Therefore, there is no need to run this script.",
          "Press enter to exit.", sep="\n")
    input()
    sys.exit()

# The first of the main loops. Make new sections in this loop.
if buildnumber <= 102:
    therearenoconfigchanges = "you bet"
if buildnumber <= 101:
    therearenoconfigchanges = "you bet"
if buildnumber <= 100:
    therearenoconfigchanges = "you bet"
if buildnumber <= 63:
    therearenoconfigchanges = "you bet"
if buildnumber <= 62:
    try:
        config.add_section("GEOCODER API")
    except configparser.DuplicateSectionError:
        print("Failed to add the geocoder API section.")

    try:
        config.add_section("FAVORITE LOCATIONS")
    except configparser.DuplicateSectionError:
        print("Failed to add the favorite locations section. Does it exist?")

    try:
        config.add_section("FIRSTINPUT")
    except configparser.DuplicateSectionError:
        print("Failed to add the firstinput section. Does it exist?")

    try:
        config.add_section("HURRICANE")
    except configparser.DuplicateSectionError:
        print("Failed to add the hurricane section. Does it exist?")

if buildnumber <= 61:
    try:
        config.add_section("PREFETCH")
    except configparser.DuplicateSectionError:
        print("Failed to add the prefetch section. Does it exist?")

if buildnumber <= 60:
    try:
        config.add_section("CACHE")
    except configparser.DuplicateSectionError:
        print("Failed to add the cache section. Does it exist?")

    try:
        config.add_section("RADAR GUI")
    except configparser.DuplicateSectionError:
        print("Failed to add the radar GUI section. Does it exist?")

    try:
        config.add_section("GEOCODER")
    except configparser.DuplicateSectionError:
        print("Failed to add the geocoder section. Does it exist?")

# Second loop. Add new configuration options.

if buildnumber <= 102:
    therearenoconfigchanges = "you bet"
if buildnumber <= 101:
    therearenoconfigchanges = "you bet"
if buildnumber <= 100:
    config['FIRSTINPUT']['allow_latlonqueries'] = 'True'
    config['FIRSTINPUT']['allow_w3wqueries'] = 'False'
    config['GEOCODER API']['customkey_w3w'] = 'None'
    # For this variable we need to specify that this should only be set between build nums 61 - 100
    if 61 <= buildnumber <= 100:
        try:
            config['CACHE']['36hourly_cachedtime'] = config['CACHE']['threedayhourly_cachedtime']
        except KeyError:
            config['CACHE']['36hourly_cachedtime'] = config['CACHE']['hourly_cachedtime']
if buildnumber <= 63:
    config['FAVORITE LOCATIONS']['favloc1_data'] = "None"
    config['FAVORITE LOCATIONS']['favloc2_data'] = "None"
    config['FAVORITE LOCATIONS']['favloc3_data'] = "None"
    config['FAVORITE LOCATIONS']['favloc4_data'] = "None"
    config['FAVORITE LOCATIONS']['favloc5_data'] = "None"
    config['FIRSTINPUT']['allow_airportqueries'] = "True"
    # Before committing new iteration variable changes, store the user's old settings.

    # Define availability vars
    old_detailedinfoloops_avail = True
    old_forecastinfoloops_avail = True
    old_alertsusloops_avail = True
    old_alertseuloops_avail = True

    try:
        old_detailedinfoloops = config['UI']['detailedinfoloops']
    except KeyError:
        old_detailedinfoloops = ""
        old_detailedinfoloops_avail = False

    try:
        old_forecastinfoloops = config['UI']['forecast_detailedinfoloops']
    except KeyError:
        old_forecastinfoloops = ""
        old_forecastinfoloops_avail = False

    try:
        old_alertsusloops = config['UI']['alerts_usiterations']
    except KeyError:
        old_alertsusloops = ""
        old_alertsusloops_avail = False

    try:
        old_alertseuloops = config['UI']['alerts_euiterations']
    except KeyError:
        old_alertseuloops = ""
        old_alertseuloops_avail = False

    # Commit the new changes to the config file based on previous user settings.

    if old_detailedinfoloops_avail is True:
        config['UI']['hourlyinfo_iterations'] = old_detailedinfoloops
    else:
        config['UI']['hourlyinfo_iterations'] = '2'

    if old_forecastinfoloops_avail is True:
        config['UI']['forecastinfo_iterations'] = old_forecastinfoloops
    else:
        config['UI']['forecastinfo_iterations'] = '3'

    if old_detailedinfoloops_avail is True:
        config['UI']['hurricaneinfo_iterations'] = old_detailedinfoloops
        config['UI']['tideinfo_iterations'] = old_detailedinfoloops
        config['UI']['historicalhourly_iterations'] = old_detailedinfoloops
        config['UI']['yesterdayhourly_iterations'] = old_detailedinfoloops
    else:
        config['UI']['hurricaneinfo_iterations'] = '2'
        config['UI']['tideinfo_iterations'] = '6'
        config['UI']['historicalhourly_iterations'] = '2'
        config['UI']['yesterdayhourly_iterations'] = '2'

    if old_alertsusloops_avail is True:
        config['UI']['alertsus_iterations'] = old_alertsusloops
    else:
        config['UI']['alertsus_iterations'] = '1'

    if old_alertseuloops_avail is True:
        config['UI']['alertseu_iterations'] = old_alertseuloops
    else:
        config['UI']['alertseu_iterations'] = '2'

    config['GEOCODER API']['skipverification'] = 'False'

if buildnumber <= 62:
    config['FIRSTINPUT']['geoipservice_enabled'] = 'False'
    config['FIRSTINPUT']['allow_pwsqueries'] = 'True'
    config['HURRICANE']['enablenearestcity'] = 'False'
    config['HURRICANE']['enablenearestcity_forecast'] = 'False'
    config['HURRICANE']['api_username'] = 'pyweather_proj'
    config['HURRICANE']['nearestcitysize'] = 'medium'
    config['FAVORITE LOCATIONS']['enabled'] = 'True'
    config['FAVORITE LOCATIONS']['favloc1'] = 'None'
    config['FAVORITE LOCATIONS']['favloc2'] = 'None'
    config['FAVORITE LOCATIONS']['favloc3'] = 'None'
    config['FAVORITE LOCATIONS']['favloc4'] = 'None'
    config['FAVORITE LOCATIONS']['favloc5'] = 'None'
    config['GEOCODER API']['customkey_enabled'] = 'False'
    config['GEOCODER API']['customkey'] = 'None'
    config['SUMMARY']['showyesterdayonsummary'] = 'False'
    config['PREFETCH']['yesterdaydata_atboot'] = 'False'
    config['CACHE']['yesterday_cachedtime'] = '720'
    config['UI']['extratools_enabled'] = 'False'

if buildnumber <= 61:
    config['CACHE']['tide_cachedtime'] = '480'
    config['SUMMARY']['showtideonsummary'] = 'False'
    config['CACHE']['36hourly_cachedtime'] = '60'
    config['CACHE']['tendayhourly_cachedtime'] = '60'
    config['PREFETCH']['10dayfetch_atboot'] = 'False'
    config['PREFETCH']['hurricanedata_atboot'] = 'False'
    config['CACHE']['hurricane_cachedtime'] = '180'

if buildnumber <= 60:
    config['CACHE']['alerts_cachedtime'] = '5'
    config['CACHE']['current_cachedtime'] = '10'
    config['CACHE']['36hourly_cachedtime'] = '60'
    config['CACHE']['tendayhourly_cachedtime'] = '60'
    config['CACHE']['forecast_cachedtime'] = '60'
    config['CACHE']['almanac_cachedtime'] = '240'
    config['CACHE']['sundata_cachedtime'] = '480'
    config['RADAR GUI']['radar_imagesize'] = 'normal'
    config['RADAR GUI']['bypassconfirmation'] = 'False'
    config['CACHE']['enabled'] = 'True'
    config['SUMMARY']['showAlertsOnSummary'] = 'True'


# Print out new config changes, removals will come after this

print("New configuration options have been added in the version of PyWeather you're upgrading to.")

if buildnumber == 101 or buildnumber == 102:
    print("No configuration changes have been made.")
if buildnumber <= 100:
    print("FIRSTINPUT/allow_latlonqueries - Defaults to True",
          "Sets if PyWeather will allow direct lat/lon queries.", sep="\n")
    print("FIRSTINPUT/allow_w3wqueries - Defaults to False",
          "Sets if PyWeather will allow what3words queries.", sep="\n")
    print("GEOCODER API/customkey_w3w - Defaults to None",
          "Sets a custom API key for the what3words geocoder", sep="\n")
    if 61 <= buildnumber <= 100:
        print("CACHE/36hourly_cachedtime - Renamed (defaults to 60)",
              "This is a rename of CACHE/threedayhourly_cachedtime.",
              "Your settings for 36-hr hourly cached time will remain.", sep="\n")
if buildnumber <= 63:
    print("FAVORITE LOCATIONS/favloc1_data - Defaults to None",
          "Sets extra data for favorite location 1.", sep="\n")
    print("FAVORITE LOCATIONS/favloc2_data - Defaults to None",
          "Sets extra data for favorite location 2.", sep="\n")
    print("FAVORITE LOCATIONS/favloc3_data - Defaults to None",
          "Sets extra data for favorite location 3.", sep="\n")
    print("FAVORITE LOCATIONS/favloc4_data - Defaults to None",
          "Sets extra data for favorite location 4.", sep="\n")
    print("FAVORITE LOCATIONS/favloc5_data - Defaults to None",
          "Sets extra data for favorite location 5.", sep="\n")
    print("FIRSTINPUT/allow_airportqueries - Defaults to True",
          "Sets if PyWeather will allow airport queries.", sep="\n")
    print("UI/hourlyinfo_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for hourly data.", sep="\n")
    print("UI/forecastinfo_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for forecast data.", sep="\n")
    print("UI/hurricaneinfo_iterations - Defaults to previous user settings",
          'Sets iterations needed for continue prompt to appear for hurricane data.', sep="\n")
    print("UI/tideinfo_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for tide data.", sep="\n")
    print("UI/historicalhourly_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for historical hourly data.", sep="\n")
    print("UI/yesterdayhourly_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for yesterday hourly data.", sep="\n")
    print("UI/alertsus_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for US alerts data", sep="\n")
    print("UI/alertseu_iterations - Defaults to previous user settings",
          "Sets iterations needed for continue prompt to appear for EU alert data", sep="\n")
    print("GEOCODER API/skipverification - Defaults to False",
          "Sets if the custom geocoder key should be verified at boot.", sep="\n")

if buildnumber <= 62:
    print("FIRSTINPUT/geoipservice_enabled - Defaults to False",
          "Sets if the service for current location queries is enabled.", sep="\n")
    print("FIRSTINPUT/allow_pwsqueries - Defaults to True",
          "Sets if PyWeather will allow PWS queries.", sep="\n")
    print("HURRICANE/enablenearestcity - Defaults to False",
          "Sets if the nearest city feature for hurricanes is enabled.", sep="\n")
    print("HURRICANE/enablenearestcity_forecast - Defaults to False",
          "Sets if the nearest city feature for hurricane forecasts is enabled.", sep="\n")
    print("HURRICANE/api_username - Defaults to 'pyweather_proj'",
          "Sets the API username for the hurricane nearest city feature.", sep="\n")
    print("HURRICANE/nearestcitysize - Defaults to 'medium'",
          "Sets the threshold for a city to appear as a nearest city.", sep="\n")
    print("FAVORITE LOCATIONS/favloc1 - Defaults to None",
          "Sets the first favorite location.", sep="\n")
    print("FAVORITE LOCATIONS/favloc2 - Defaults to None",
          "Sets the second favorite location.", sep="\n")
    print("FAVORITE LOCATIONS/favloc3 - Defaults to None",
          "Sets the third favorite location.", sep="\n")
    print("FAVORITE LOCATIONS/favloc4 - Defaults to None",
          "Sets the fourth favorite location.", sep="\n")
    print("FAVORITE LOCATIONS/favloc5 - Defaults to None",
          "Sets the fifth favorite location.", sep="\n")
    print("GEOCODER API/customkey_enabled - Defaults to False",
          "Sets if a custom API key for the geocoder is enabled.", sep="\n")
    print("GEOCODER API/customkey - Defaults to None",
          "Sets the custom API key for the geocoder, if enabled.", sep="\n")
    print("SUMMARY/showyesterdayonsummary - Defaults to False",
          "Sets if yesterday's weather data is on the summary screen.", sep="\n")
    print("PREFETCH/yesterdaydata_atboot - Defaults to False",
          "Sets if yesterday's weather data should be prefetched at boot.", sep="\n")
    print("CACHE/yesterday_cachedtime - Defaults to '720'",
          "Sets the cache time for yesterday's weather data", sep="\n")
    print("UI/extratools_enabled - Defaults to False",
          "Sets if the extra tools functionality is enabled.", sep="\n")
if buildnumber <= 61:
    print("CACHE/tide_cachedtime - Defaults to '480'",
          "Sets the cache time for tide data.", sep="\n")
    print("SUMMARY/showtideonsummary - Defaults to False",
          "Sets if tide data should be shown on the summary screen.", sep="\n")
    print("CACHE/threedayhourly_cachedtime - Defaults to '60'",
          "Sets the cache time for 1.5 day hourly data.", sep="\n")
    print("CACHE/tendayhourly_cachedtime - Defaults to '60'",
          "Sets the cache time for 10 day hourly data.", sep="\n")
    print("CACHE/hurricane_cachedtime - Defaults to '180'",
          "Sets the cache time for hurricane data.", sep="\n")
    print("PREFETCH/10dayfetch_atboot - Defaults to False",
          "Sets if 10-day hourly data is fetched at boot", sep="\n")
    print("PREFETCH/hurricanedata_atboot - Defaults to False",
          "Sets if hurricane data is prefetched at boot.", sep="\n")
if buildnumber <= 60:
    print("CACHE/alerts_cachedtime - Defaults to '5'",
          "Sets the cache time for alert data", sep="\n")
    print("CACHE/current_cachedtime - Defaults to '10'",
          "Sets the cache time for current weather data", sep="\n")
    print("CACHE/36hourly_cachedtime - Defaults to '60'",
          "Sets the cache time for 1.5 day hourly data", sep="\n")
    print("CACHE/tendayhourly_cachedtime - Defaults to '60'",
          "Sets the cache time for 10 day hourly data", sep="\n")
    print("CACHE/forecast_cachedtime - defaults to '60'",
          "Sets the cache time for forecast data.", sep="\n")
    print("CACHE/almanac_cachedtime - Defaults to '240'",
          "Sets the cache time for almanac data.", sep="\n")
    print("CACHE/sundata_cachedtime - Defaults to '480'",
          "Sets the cache time for sunrise data.", sep="\n")
    print("CACHE/enabled - Defaults to True",
          "Sets if the cache system is enabled.", sep="\n")
    print("RADAR GUI/radar_imagesize - Defaults to 'normal'",
          "Sets the image size for radar images in the radar GUI.", sep="\n")
    print("RADAR GUI/bypassconfirmation - Defaults to False",
          "Sets if the radar experimental warning can be bypassed.", sep="\n")
    print("SUMMARY/showAlertsOnSummary - Defaults to True",
          "Sets if alerts data should be shown on summary.", sep="\n")

# Print out removed sections & config options
print("")
print("Some configuration options and sections are no longer used in PyWeather.",
      "These will be automatically deleted.", sep="\n")

if buildnumber == 100 or buildnumber == 101 or buildnumber == 102:
    print("No configuration options and sections have been removed.")
if buildnumber <= 63:
    print("UI/detailedinfoloops - Removed",
          "Used to set iterations before continue prompt appears for many data types.", sep="\n")
    print("UI/forecast_detailedinfoloops - Removed",
          "Used to set iterations before continue prompt appears for forecast data.", sep="\n")
    print("UI/alerts_usiterations - Removed",
          "Used to set iterations before continue prompt appears for US alerts data.", sep="\n")
    print("UI/alerts_euiterations - Removed",
          "Used to set iterations before continue prompt appears for EU alerts data.", sep="\n")
    if buildnumber == 62 or buildnumber == 63:
        print("GEOCODER/scheme - Removed",
              "Used to set the geocoder scheme.", sep="\n")
        print("GEOCODER section - Removed")

    if 61 <= buildnumber <= 100:
        print("CACHE/threedayhourly_cachedtime - Renamed",
              "Used to set cache time for 1.5 day hourly. Renamed due to confusion.", sep="\n")

    print("VERBOSITY/keybackup_verbosity - Removed",
          "Used to set if verbosity for the keybackup script was enabled.", sep="\n")
    print("VERBOSITY/configdefault_verbosity - Removed",
          "Used to set if verbosity for the configdefault script was enabled.", sep="\n")
    print("TRACEBACK/configdefault_verbosity - Removed",
          "Used to set if tracebacks for the configdefault script was enabled.", sep="\n")

if buildnumber <= 62:
    print("UPDATER/allowGitForUpdating - Removed",
          "Used to set if the Git updater was enabled", sep="\n")
if buildnumber <= 61:
    print("CACHE/hourly_cachedtime - Removed",
          "Used to set the global hourly cache time", sep="\n")
    print("HOURLY/10dayhourly_atboot - Removed",
          "Used to set if 10-day hourly data was prefetched at boot.", sep="\n")
    print("HOURLY section - No longer in use.")
    print("CHANGELOG section - No longer in use.")

print("")

if buildnumber <= 100:
    if 61 <= buildnumber <= 100:
        try:
            config.remove_option('CACHE', 'threedayhourly_cachedtime')
        except (configparser.NoOptionError, configparser.NoSectionError):
            print("Failed to automatically remove CACHE/threedayhourly_cachedtime")


if buildnumber <= 63:
    try:
        config.remove_option('UI', 'detailedinfoloops')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove UI/detailedinfoloops.")

    try:
        config.remove_option('UI', 'forecast_detailedinfoloops')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove UI/forecast_detailedinfoloops.")

    try:
        config.remove_option('UI', 'alerts_usiterations')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove UI/alerts_usiterations.")

    try:
        config.remove_option('UI', 'alerts_euiterations')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove UI/alerts_euiterations.")

    if buildnumber == 62 or buildnumber == 63:
        try:
            config.remove_option('GEOCODER', 'scheme')
        except (configparser.NoOptionError, configparser.NoSectionError):
            print("Failed to automatically remove GEOCODER/scheme")

    if buildnumber == 62 or buildnumber == 63:
        try:
            config.remove_section('GEOCODER')
        except configparser.NoSectionError:
            print("Failed to automatically remove section GEOCODER")

    try:
        config.remove_option('VERBOSITY', 'keybackup_verbosity')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove VERBOSITY/keybackup_verbosity")

    try:
        config.remove_option('VERBOSITY', 'configdefault_verbosity')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove VERBOSITY/configdefault_verbosity")

    try:
        config.remove_option('TRACEBACK', 'configdefault_tracebacks')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove TRACEBACK/configdefault_tracebacks")


if buildnumber <= 62:
    try:
        config.remove_option('UPDATER', 'allowGitForUpdating')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove UPDATER/allowGitForUpdating")

if buildnumber <= 61:
    try:
        config.remove_option('CACHE', 'hourly_cachedtime')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove CACHE/hourly_cachedtime")

    try:
        config.remove_option('HOURLY', '10dayhourly_atboot')
    except (configparser.NoOptionError, configparser.NoSectionError):
        print("Failed to automatically remove HOURLY/10dayhourly_atboot")

    try:
        config.remove_section('HOURLY')
    except configparser.NoSectionError:
        print("Failed to automatically remove section HOURLY")

    try:
        config.remove_section('CHANGELOG')
    except configparser.NoSectionError:
        print("Failed to automatically remove section CHANGELOG")


# Run functions for geopy detection, etc.

print("")

if buildnumber <= 63:
    newlibinstaller(library="click")
    print("")
    newlibinstaller(library="certifi")
if buildnumber <= 62:
    newlibinstaller(library="halo")
    print("")
    newlibinstaller(library="click")
    print("")
    newlibinstaller(library="certifi")
    print("")

# PyWeather 0.6.3 beta only - Check for odd config values from a critical bug workaround.
if buildnumber == 63:
    # Get float vars for 1.5 day and almanac cache times
    onepoint5day_cachetime = float(config['CACHE']['36hourly_cachedtime'])
    almanac_cachetime = float(config['CACHE']['almanac_cachedtime'])

    if (onepoint5day_cachetime >= 720 and onepoint5day_cachetime != 3600) or (almanac_cachetime >= 1440 and almanac_cachetime != 14400):
        cachetimefix()

    if onepoint5day_cachetime == 3600:
        config['CACHE']['36hourly_cachedtime'] = '60'

    if onepoint5day_cachetime == 14400:
        config['CACHE']['almanac_cachedtime'] = '240'

# Finally, update the version info file & commit to the config file.

try:
    with open('storage//config.ini', 'w') as configfile:
            config.write(configfile)
    print("Configuration options committed successfully!")
except:
    print("Couldn't update your config file! A full error will be printed below.")
    traceback.print_exc()
    print("Please report this bug to GitLab (gitlab.com/o355/pyweather), along with",
          "the full error. Along with that, please manually add the configuration entries",
          "as listed above, with their default values in your configuration file.",
          "Alternatively, delete your config file, and run configsetup.py",
          "Press enter to exit.")
    input()
    try:
        open("updater//versioninfo.txt", 'w').close()
        with open("updater//versioninfo.txt", 'a') as out:
            out.write("v1.0.3")
            out.close()
    except:
        print("Could not write out an updated versioninfo text file. Please",
              "modify 'updater/versioninfo.txt' to display 'v1.0.3'.", sep="\n")
    sys.exit()
try:
    open("updater//versioninfo.txt", 'w').close()
    with open("updater//versioninfo.txt", 'a') as out:
        out.write("v1.0.3")
        out.close()
except:
    print("Could not write out an updated versioninfo text file. Please",
          "modify 'updater/versioninfo.txt' to display 'v1.0.3'.", sep="\n")

print("")
print("Ta-da! PyWeather is all up-to-date. Enjoy the new features and bug fixes!",
      "Press enter to exit.", sep="\n")
input()
sys.exit()